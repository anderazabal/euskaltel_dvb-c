Obtención de datos de una señal DVB-C - Euskaltel
=================================================

He publicado estos archivos para que sirva de referencia a aquellas personas interesadas en conocer cómo funciona la emisión a través de DVB-C de señales de TV digital.

Recientemente he conseguido hacerme con un sintonizador DVB-C compatible, por lo que por fin he podido trastear con la señal que emite mi proveedor de telecomunicaciones en un PC.

Esta información también puede resultar de interés para aquellas personas que busquen los datos de los diferentes servicios (canales de tv) que emite Euskaltel, datos tales como:

- Frecuencia
- Modulación
- Tasa de símbolos (baudios)

con el fin de poder sintonizar los canales en el decodificador o en el propio televisor (aquel que sea DVB-C compatible, lo habitual es que sean sólo DVB-T). En [los foros de euskaltel][] no paro de leer a gente que pide datos de estas frecuencias, por lo que creo que puede resultar útil esta información. En tal caso, lo más rápido es consultar el archivo [`services_list_dvbv5-scan_default_output.txt`][], pero, si eres curioso, querrás saber cómo he obtenido esta información sin tener que recurrir a [los foros de euskaltel][] .

El sintonizador que uso es un **TT-TVStick CT2-4400 v2** de la marca TechnoTrend, para más información, consulta los siguientes enlaces:

- http://engl.technotrend.eu/2990/TT-TVStick__CT2-4400.html

- https://www.linuxtv.org/wiki/index.php/TechnoTrend_TT-TVStick_CT2-4400

Se puede encontrar información de buena parte de lo que se habla de aquí [en este enlace][].

`NOTA:` utilizo Arch - distribución de GNU/Linux - para obtener todos estos datos. Todos los comandos, nombres de programa e incluso el final de línea de los archivos de texto son los propios de este sistema (si descargas algo en Windows aconsejo `Notepad++` para visualizarlo). Desconozco si todos estos programas están en las distribuciones GNU/Linux habituales, supongo que sí, pero no puedo asegurarlo.

-----------------
networks_list.txt
-----------------

    w_scan -f c -c ES -x > networks_list.txt

Contiene la lista de los distintos [TS][] (Transport Streams MPEG-2), más conocidos como `mux` o `multiplex`. Personalmente prefiero guardarla ordenada. Para no destrozar la cabecera, ejecuta esto:

    head -n 11 networks_list.txt > networks_list_sorted.txt
    (tail -n +12 networks_list.txt | sort) >> networks_list_sorted.txt
    mv networks_list_sorted.txt networks_list.txt

    
---------------------
w_scan_raw_output.txt
---------------------

    w_scan -f c -c ES &> w_scan_raw_output.txt

Contiene la salida (stdout + stderr) del comando `w_scan` anterior, devolviendo bastante información sobre los [ES][] (Elementary Streams MPEG-2) detectados.


----------------------
services_list_scan.txt
----------------------

    scan -o zap -A 2 -C ISO-8859-15 networks_list.txt > services_list_scan.txt

Contiene la lista de los servicios MPEG-2 detectados y de los [PID]s de los [ES][] que componen cada uno de ellos en formato channels.conf, compatible con aplicaciones [linuxtv dvb-apps][] . Esta salida no diferencia de qué tipo es cada uno de los [ES][] detectados.


-------------------
scan_raw_output.txt
-------------------

    scan -v -v -o zap -A 2 -C ISO-8859-15 networks_list.txt &> scan_raw_output.txt

Contiene la salida (stdout + stderr) del comando `scan` anterior. En esta se puede encontrar cierta información muy básica de las tablas [PAT][], [PMT][], [NIT][] y [SDT][].

- **Tablas propias y obligatorias del estándar MPEG-2:**
    - [**PAT**][]: Program Association Table: Muestra los [PID][] de las tablas [PMT][] de los diferentes servicios presentes en el [TS][]. Siempre tiene [PID][] 0x00.
    - [**PMT**][]: Program Map Table: Una tabla por cada servicio presente. Muestra [PID][], tipo de trama y demás descriptores de cada [ES][]. Los [PID][] de cada tabla son asignados por la [PAT][].

- **Tablas propias y obligatorias del estándar DVB:**
    - [**NIT**][]: Network Information Table: Muestra información de la red. Siempre será el programa #0 de la [PAT][].
    - [**SDT**][]: Service Description Table: Al igual que la [PMT][], muestra información sobre los servicios presentes, pero a diferencia de las [PMT][], sólo existe una tabla [SDT][] con la información de todos los servicios. Esta información está más bien orientada al usuario, en esta tabla podremos encontrar datos como el nombre del servicio, si está emitiendo o no, si está codificado o no...
  
  
-------------------------------------------    
services_list_dvbv5-scan_default_output.txt
-------------------------------------------

    dvbv5-scan -C ES -I CHANNEL -o services_list_dvbv5-scan_default_output.txt networks_list.txt

Al igual que `scan`, `dvbv5-scan` devuelve la lista de los Services MPEG-2 detectados incluyendo los [PID][] de los [ES][]. Esta lista está en formato compatible con las nuevas aplicaciones [dvbv5][], formato que diferencia los [PID][] de los [ES][] según sea su tipo (audio, video...). Este formato no es compatible con los habituales formatos de las [linuxtv dvb-apps][].


---------------------------------------
services_list_dvbv5-scan_zap_output.txt
---------------------------------------

    dvbv5-scan -C ES -I CHANNEL -O ZAP -o services_list_dvbv5-scan_zap_output.txt networks_list.txt

Contiene la lista de los Services MPEG-2 detectados incluyendo los [PID][] de los [ES][]. Esta lista está en formato zap que es compatible con [linuxtv dvb-apps][].


-------------------------------------------
services_list_dvbv5-scan_channel_output.txt
-------------------------------------------

    dvbv5-scan -C ES -I CHANNEL -O CHANNEL -o services_list_dvbv5-scan_channel_output.txt networks_list.txt

Contiene la lista de los Services MPEG-2 detectados incluyendo los [PID][] de los [ES][]. Esta lista está en formato channel, compatible con [linuxtv dvb-apps][].


-------------------------
dvbv5-scan_raw_output.txt
-------------------------

    dvbv5-scan -v -C ES -I CHANNEL networks_list.txt &> dvbv5-scan_raw_output.txt

Contiene la salida (stdout + stderr) del comando `dvbv5-scan` anterior. Se pueden ver las siguientes tablas [SI][] (Service Information):

- **Tablas propias y obligatorias del estándar MPEG-2:**
    - [**PAT**][]: Program Association Table: Muestra los [PID][] de las tablas [PMT][] de los diferentes servicios presentes en el [TS][]. Siempre tiene [PID][] 0x00.
    - [**PMT**][]: Program Map Table: Una tabla por cada servicio presente. Muestra [PID][], tipo de trama y demás descriptores de cada [ES][]. Los [PID][] de cada tabla son asignados por la [PAT][].

- **Tablas propias y obligatorias del estándar DVB:**
    - [**NIT**][]: Network Information Table: Muestra información de la red. Siempre será el programa #0 de la [PAT][].
    - [**SDT**][]: Service Description Table: Al igual que la [PMT][], muestra información sobre los servicios presentes, pero a diferencia de las [PMT][], sólo existe una tabla [SDT][] con la información de todos los servicios. Esta información está más bien orientada al usuario, en esta tabla podremos encontrar datos como el nombre del servicio, si está emitiendo o no, si está codificado o no...

A parte de este archivo, `dvbv5-scan` genera un archivo llamado `dvb_channel.conf`, si ya tenemos `services_list_dvbv5-scan_default_output.txt` no nos hará falta pues son similares. Lo podremos borrar con:

    rm dvb_channel.conf


-------------------    
epg-info_epgrab.txt
-------------------

Contiene la información [EPG][] (Electronic Program Guide) extraída con epgrab. En el caso de Euskaltel, está codificada con el mapa de caracteres ISO-8859-15, la salida que obtendremos ya estará en UTF-8.

Como el archivo [`epg-data_epgrab.txt`][] que he generado en mis pruebas rondaba los 10 Mb, he decido comprimirlo. Para descomprimirlo, ejecuta:

    gunzip epg-info_epgrab.txt.gz
    
o utiliza tu gestor de archivos comprimidos favorito.

Para generar esta salida, he utilizado el archivo [`chanidents`][]. Este archivo lo he generado utilizando los datos del archivo [`services_list_dvbv5-scan_zap_output.txt`][], en concreto he copiado el último dato de cada fila que corresponde al [PID][] del servicio y despues el primer dato, que corresponde al nombre del canal. A ambos campos los he separado con un tabulador. En el caso del nombre del canal he cambiado los espacios por `_`, ya que parece que `epgrab` no se lleva bien con ellos. Me he basado en [este ejemplo del mantenedor del repositorio de `epgrab`][] para generarlo. Este comando tan corto es el necesario para llegar a este resultado:

    paste <(cat services_list_dvbv5-scan_zap_output.txt | rev | cut -d ':' -f 1 | rev) <(cut -d ':' -f 1 services_list_dvbv5-scan_zap_output.txt | tr -s ' ' '_') > chanidents

Una vez obtenido el archivo [`chanidents`][], lo he copiado en el mismo directorio con el nombre [`channels.conf`][]:

    cp chanidents channels.conf

`epgrab` utilizará estos dos archivos para parsear el id del canal y cambiarlo por un nombre. De esta manera es más sencillo conocer el nombre del canal al que pertenece cada entrada.

Usando estos archivos, la salida tendrá este aspecto:

    <programme channel="DISCOVERY_MAX" start="20170204130000 +0100" stop="20170204140000 +0100">

Si no se utilza estos dos archivos, tendremos salidas como esta:

    <programme channel="7080.dvb.guide" start="20170204130000 +0100" stop="20170204140000 +0100">

`7080.dvb.guide` no dice mucho, la verdad.

Antes de ejecutar `epgrab` es necesario sintonizar algún canal, da igual cuál. En caso contrario, `epgrab` se quejará devolviendo esto:

    DMX_SET_FILTER:: Invalid argument
    Unable to get event data from multiplex.

Para sintonizar he utilizado el siguiente proceso:

    czap -c services_list_dvbv5-scan_zap_output.txt "ANTENA 3" &> /dev/null &
    
`&> /dev/null` evitará que nos ensucie la pantalla con continuos mensajes de que está sintonizado y `&` al final nos devuelve el prompt, así podremos seguir ejecutando el comando `epgrab`.

Ahora ya estamos en condiciones de ejecutar `epgrab`, vamos a ello:

    epgrab -f epg-info_epgrab.txt -c -e ISO-8859-15
    
Una vez termine el proceso y se obtenga el archivo con la información [EPG][], ejecutar `fg` para volver al proceso `czap` abierto antes y poder así terminarlo con la combinación de teclas **CTRL+C**. Esto es lo que veremos:

    user@arch ~ $ fg
    czap -c services_list_dvbv5-scan_zap_output.txt "ANTENA 3" &> /dev/null
    (pulsar ahora CTRL+C)
    user@arch ~ $

Ahora el sintonizador queda libre para que cualquier otro proceso lo pueda utilizar.


--------------------
Programas utilizados
====================

w_scan
------

Con `w_scan` podremos obtener las frecuencias de la transmisión DVB. `w_scan` es único haciendo este trabajo y complementan a otras utilidades como `scan`, `dvbscan` `dvb5-scan`... Esta últimas no serán capaces por si mismas de obtener los datos que devuelve `w_scan`, es por esto que el primer archivo que he listado es [`networks_list.txt`][], este contiene las frecuencias a las que el operador emite los [TS][]. Conocidos como muxes o multiplex, agrupan en un solo flujo diferentes [ES][] que a su vez conformarán los diferentes servicios (los canales de la tele, vamos...)

En mi caso, utilizo ArchLinux por lo que no me he tenido que complicar mucho la vida, para instalarlo he ejecutado:
    
    yaourt -S w_scan

y a correr!.

Enlaces web:

- https://linuxtv.org/wiki/index.php/W_scan

- http://wirbel.htpc-forum.de/w_scan/index_en.html

- https://linuxtv.org/wiki/index.php/Frequency_scan#Comparison_of_DVB_frequency_scanning_commandline_utilities


----
scan
----

`scan`, como su nombre indica, escanea las frecuencias que le pasamos con el archivo [`networks_list.txt`][] como argumento devolviendo una lista con los servicios detectados en formato [`channels.conf`][]. El desarrollo de scan al parecer está discontinuado.

Para instalarlo, en ArchLinux:

    pacman -S linuxtv-dvb-apps
    
Como ejemplo de la salida que genera scan he publicado el archivo [`services_list_scan.txt`][].

Enlaces web:

- https://linuxtv.org/wiki/index.php/Scan

- https://linuxtv.org/wiki/index.php/Frequency_scan#Comparison_of_DVB_frequency_scanning_commandline_utilities


----------
dvbv5-scan
----------

`dvbv5-scan` es el nuevo escáner del grupo linuxtv.org, como novedades soporta DVB-S2, DVB-T2, DVB-C Annex C, ISDB-T y otros siempre que sean compatibles con la versión 5 de la [API DVB][]. Utiliza un nuevo formato de archivos compatible con todas las aplicaciones [dvbv5][] que soporta todos los tipos de estándares de TV digital. Soporta retrocompatibilidad con los anteriores formatos utilizados por las [linuxtv dvb-apps][].

Para disponer de esta aplicación hay que instalar las v4l-utils, en Arch:

    yaourt -S v4l-utils
    

Como ejemplos de los formatos que maneja dvbv5-scan, he publicado los siguientes archivos:

    services_list_dvbv5-scan_default_output.txt
    services_list_dvbv5-scan_channel_output.txt
    services_list_dvbv5-scan_zap_output.txt

Enlaces web:

- https://linuxtv.org/wiki/index.php/Dvbv5-scan
    
- https://git.linuxtv.org/v4l-utils.git/about/
    
- https://linuxtv.org/wiki/index.php/Frequency_scan#Comparison_of_DVB_frequency_scanning_commandline_utilities


------
epgrab
------

Esta utilidad permite obtener información [EPG][] (Electronic Program Guide) de una señal DVB en formato XMLTV. He publicado el archivo [`epg-data_epgrab.txt`][] a modo de ejemplo.

Para instalar `epgrab`, he clonado el [repositorio git del proyecto][]. Para compilarlo y una vez dentro del directorio del repositorio clonado, ejecuta lo siguiente:

    cmake .
    make
    cp epgrab /usr/bin/


[`services_list_dvbv5-scan_default_output.txt`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/services_list_dvbv5-scan_default_output.txt
[`services_list_dvbv5-scan_zap_output.txt`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/services_list-dvbv5-scan_zap_output.txt
[`chanidents`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/chanidents
[`channels.conf`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/channels.conf
[este ejemplo del mantenedor del repositorio de `epgrab`]: https://github.com/hiroshiyui/epgrab/blob/master/chanidents
[`networks_list.txt`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/networks_list.txt
[`epg-data_epgrab.txt`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/epg-info_epgrab.txt.gz
[`services_list_scan.txt`]: https://gitlab.com/anderazabal/euskaltel_dvb-c/blob/master/services_list_scan.txt
[linuxtv dvb-apps]: https://www.linuxtv.org/wiki/index.php/LinuxTV_dvb-apps
[dvbv5]: https://www.linuxtv.org/wiki/index.php/DVBv5_Tools
[los foros de Euskaltel]: http://foro.euskaltel.com/Forum-Telebista-Televisi%C3%B3n
[repositorio git del proyecto]: https://github.com/hiroshiyui/epgrab
[**ES**]: https://en.wikipedia.org/wiki/Elementary_stream
[ES]: https://en.wikipedia.org/wiki/Elementary_stream
[**TS**]: https://es.wikipedia.org/wiki/Transport_Stream
[TS]: https://es.wikipedia.org/wiki/Transport_Stream
[**PAT**]: https://es.wikipedia.org/wiki/MPEG-PSI#PAT
[PAT]: https://es.wikipedia.org/wiki/MPEG-PSI#PAT
[**PMT**]: https://es.wikipedia.org/wiki/MPEG-PSI#PMT
[PMT]: https://es.wikipedia.org/wiki/MPEG-PSI#PMT
[**NIT**]: https://es.wikipedia.org/wiki/MPEG-PSI#NIT
[NIT]: https://es.wikipedia.org/wiki/MPEG-PSI#NIT
[**SDT**]: https://en.wikipedia.org/wiki/Service_Description_Table
[SDT]: https://en.wikipedia.org/wiki/Service_Description_Table
[EPG]: https://es.wikipedia.org/wiki/Guía_electrónica_de_programas
[SI]: https://es.wikipedia.org/wiki/DVB-SI
[PID]: http://wikitel.info/wiki/MPEG-2_TS_(DVB-SI)#Packet_Identifier_.28PID.29:
[API DVB]: https://linuxtv.org/downloads/v4l-dvb-apis/uapi/dvb/dvbapi.html
[en este enlace]: http://www.interactivetvweb.org/tutorials/dtv_intro/dtv_intro